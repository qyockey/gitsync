#!/bin/bash

# error codes
readonly SUCCESS=0;
readonly FAIL=1;
readonly NO_REPLY=1
readonly NO_INTERNET=2

echo -n 'testing for internet connection...'
ping -c 1 gitlab.com > /dev/null
case $? in 
  $NO_REPLY)
    echo 'no reply from gitlab.com, exiting'
    exit $FAIL
    ;;
  $NO_INTERNET)
    echo 'no internet connection, exiting'
    exit $FAIL
    ;;
esac
echo 'connection established with gitlab.com'

run_git_cmd() {
  local command=$1
  local description=$2
  echo -n "$description..."
  eval $command
  if (( $? == $FAIL )); then 
    echo "error running '$command'"
    exit $FAIL
  fi
  echo 'succes!'
}

# number of local files with changes
LOCAL_FILES_CHANGED=$(git diff --name-only | wc --lines)
if (( LOCAL_FILES_CHANGED == 0 )); then
  echo 'no local changes detected'
else
  echo 'local changes detected'
  run_git_cmd 'git stash --quiet' 'stashing local changes'
fi
run_git_cmd 'git fetch --all --quiet --prune' 'fetching remote changes'
# number of changes to rebase onto the local repo
REMOTE_FILES_CHANGED=$(git diff --name-only origin/main | wc --lines)
echo "$REMOTE_FILES_CHANGED remote changes detected"
# exit if no changes available
if (( REMOTE_FILES_CHANGED == 0 )); then
  echo 'project is up to date!'
  exit $SUCCESS
else
  echo "$REMOTE_FILES_CHANGED remote changes detected"
fi
run_git_cmd 'git rebase --quiet' 'rebasing remote changes onto local repo'
if (( LOCAL_FILES_CHANGED > 0 )); then
  run_git_cmd 'git stash pop --quiet' 'restoring local changes'
fi
echo 'updates successfully applied!'
exit $SUCCESS
